import { Component } from "@angular/core";
import { NavController, Refresher, reorderArray } from "ionic-angular";
import { ANIMALES } from "../../data/data.animales";
import { Animal } from "../../interfaces/animal.interface";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {
  animales: Animal[] = [];
  audio = new Audio();
  audioTiempo: any;
  ordenando = false;

  constructor(public navCtrl: NavController) {
    this.animales = ANIMALES.slice(0);
  }

  reproducir(animal: Animal) {
    this.pausarAudio(animal);

    if (animal.reproduciendo) {
      animal.reproduciendo = false;
      return;
    }

    this.audio.src = animal.audio;
    this.audio.load();
    this.audio.play();

    animal.reproduciendo = true;

    this.audioTiempo = setTimeout(() => {
      animal.reproduciendo = false;
    }, animal.duracion * 1000);
  }

  private pausarAudio(animalSel: Animal) {
    clearTimeout(this.audioTiempo);

    this.audio.pause();
    this.audio.currentTime = 0;

    this.animales.forEach((animal) => {
      if (animal.nombre != animalSel.nombre) {
        animal.reproduciendo = false;
      }
    });
  }

  borrarAnimal(index: number) {
    this.animales.splice(index, 1);
  }

  recargarAnimales(refresher: Refresher) {
    console.log("Recargando los animales");

    setTimeout(() => {
      console.log("Se cargaron los animales");
      this.animales = ANIMALES.slice(0);
      refresher.complete();
    }, 2000);
  }

  reordenarAnimales(index: any) {
    this.animales = reorderArray(this.animales, index);
  }
}
